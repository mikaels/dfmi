#include <iostream>
#include <cstdlib>

#include <fstream>

#include "dynSA.h"
#include "utils.h"
#include <string.h>

using namespace std;
using namespace dynsa;

extern "C" {
#include "time.h"
#include "sys/time.h"
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
}

#include "types.h"

/**
 * Maximal length of the inserted string
 */
#define LENGTH_INS 40
/**
 * Length of the retrieved substring
 * Must be >= LENGTH_INS
 */
#define LENGTH_RETRIEVE 50

/**
 * Return the content of a file in a char *.
 * n is updated with the length of the string.
 * termin tells if we must add a terminator at the end of the string
 */
uchar *getFileContent(char *filename, size_t &n, int termin) {
  std::ifstream file(filename);
  if (!file)
    {
      cerr << "error reading file: " << filename << endl;
      exit(EXIT_FAILURE);
    }

  file.seekg(0, ios::end);
  n = file.tellg();
  if (termin)
    n++;
  file.seekg(0, ios::beg);

  uchar *text = new uchar[n+1];

  char c;
  size_t i=0;

  while (file.get(c))
    {
      text[i]=c;//(c=='\n'||c=='\r')?'X':c;
      i++;
    }
  file.close();

  if (termin)
    text[n-1]=0;
  text[n]=0;
  return text;
}

void usage(char *program) {
    cerr << "Usage : " << program << " <filename>" << endl;
    exit(1);
}

int main(int argc, char *argv[]) {
  uchar *text;
  size_t n;
  DynSA *wt;
  float *f;
  uint length_modif, pos_modif;
  uchar *pattern;

  if (argc < 2) {
    cerr << "Usage: " << argv[0] << " <filename>" << endl << endl;
    cerr << "Create an index on the text contained in the file <filename>." 
         << endl;
    cerr << "Insert a random substring in it and search for it." << endl << endl;
    exit (1);
  }

  srand(time(NULL));

  // Retrieve the text
  text = getFileContent(argv[1],n,1);

  // Creates the structure for dynamic suffix array
  wt = new DynSA();

  // Initialization of the distribution of letters
  // It is used to build a tree that is shaped for that particular letter
  // distribution.
  f = DynRankS::createCharDistribution(text, (n > 10000000) ? n+1 : 10000000, 1);
  wt->initEmptyDynSA(f);

  // Add the text to the index
  // You can also use addTextFromFile() (two parameters: filename and length)
  cout << "Adding text in the index" << endl;
  wt->addText(text,n);          

  // Add a string in the index at position p.
  // The string added is randomly taken from the original text.
  length_modif = (rand() % LENGTH_INS) + 1;
  pos_modif = (rand() % (n-1)); // positions start at 0
  pattern = new uchar[length_modif+1];
  strncpy((char *)pattern, (char *)&text[rand() % (n-length_modif)], 
          length_modif);
  pattern[length_modif]=0;

  cout << "Inserting a string of length " << length_modif << " at position "
       << pos_modif << endl;
  cout << "Inserted string is " << pattern << endl;
  wt->addChars(pattern, length_modif, pos_modif);

  // You also have deleteChars to delete a substring from the index
  // It takes two parameters: the length of the deleted substring and 
  // its position in the original text.
  //
  // Ex : to delete the inserted substring
  // wt->deleteChars(length_modif, pos_modif);


  // Searching

  // Counting the number of occurrences
  printf("There are %lu occurrences of the inserted string in the whole text\n",
         wt->count(pattern));
  
  // Retrieving locations of the occurrences
  size_t *occurrences = wt->locate(pattern);
  printf("There are %lu occurrences of the inserted string but we already knew that\n", 
         occurrences[0]);

  cout << "The positions of the occurrences are: " << endl;

  for (uint i=1; i <= occurrences[0]; i++) {
    cout << occurrences[i] << "\t";
    if (i % 5 == 0)
      cout << endl;
  }
  cout << endl;

  // Retrieving the text
  cout << "Retrieving a factor of length " << LENGTH_RETRIEVE 
       << " around the occurrences" << endl;
  uint nb_pos_before = (LENGTH_RETRIEVE - length_modif) / 2;
  for (uint i=1; i <= occurrences[0]; i++) {
    uint pos = (occurrences[i]  < nb_pos_before) ? 0 
      : occurrences[i] - nb_pos_before;
    cout << pos << "\t"
         << wt->retrieveTextFactor(pos, LENGTH_RETRIEVE) << endl;
  }
  cout << endl;

  // The whole text can be retrievede by wt->retrieveText();

  delete [] text;
  delete [] f;
  delete [] pattern;
  delete [] occurrences;

  exit(0);
}
